import {useState, useEffect, useContext} from 'react';
import {Container, Row, Col, Card, Button} from 'react-bootstrap';
import {useParams, useNavigate, Link} from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';


export default function ProductView(){
	const {user} = useContext(UserContext);
	const history = useNavigate();
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [quantity, setQuantity] = useState(0);
	const [totalAmt, setTotalAmt] = useState(0);
	const [products, setProducts] = useState([]);

	const {productId} = useParams();

	//place order lipat sa kabila page (gawa ng cart page)

	const placeOrder = (productId, quantity) => {
		fetch('https://tranquil-dusk-37263.herokuapp.com/orders/createOrder', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId: productId,
				price: price,
				quantity: quantity,
				totalAmt: totalAmt,
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data !== false) {
					setProducts([])
					

				Swal.fire({
					title: 'Successfully placed order!',
					icon: 'success',
					text: 'Thank you for ordering'
				});

				history("/products");
			} else 
			{
				Swal.fire({
					title: 'Something went wrong',
					icon: 'error',
					text: 'Please try again later'
				});

			}
		})


	};

	//get single product

	useEffect(() => {
		console.log(productId)
		fetch(`https://tranquil-dusk-37263.herokuapp.com/products/singleProduct/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
			setQuantity(data.quantity);
		})
	}, [productId]);

	return (
		<Container className ="mt-5">
			<Row>
				<Col lg={{span:6, offset:3}}>
					<Card>
						<Card.Body>
							<Card.Title>{name}</Card.Title>
							<Card.Subtitle>Description</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>Php {price}</Card.Text>							
							
							{
							 (user.isAdmin && user.id !== null)
							 ?
							 <>
							<Button variant="danger" as={Link} to={`/admin`}>Admin Dashboard</Button>
							<Link className="btn btn-danger" to="/products">Back To products</Link>
							</>
							 : 
							 <>
							<Button variant="danger" as={Link} to={`/admin`} hidden>Admin Dashboard</Button>
							<Link className="btn btn-danger" to="/products">Back To products</Link>
							</>
							}	
							{ 
							(user.id !== null && user.isAdmin !== true)
							?															
							<Button variant="primary" onClick={() => placeOrder(productId, quantity)}>Order</Button>
							:											
							<Button variant="primary" onClick={() => placeOrder(productId, quantity)} hidden>Order</Button>							
							}
							{
							(user.id === null)
							?
							<Button variant="danger" as={Link} to={`/login`}>Login</Button>
							:
							<Button variant="danger" as={Link} to={`/login`} hidden>Login</Button>
						
							}
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>

		)
}


