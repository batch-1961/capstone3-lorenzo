
import './App.css';
import AppNavBar from './components/AppNavBar';
import {Container} from 'react-bootstrap';
import Home from './pages/Home';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Register from './pages/Register';
import Products from './pages/Products';
import ProductView from './components/ProductView';
import Admin from './pages/Admin';
import Profile from './pages/Profile';
import AddProduct from './pages/AddProduct';
import UpdateProduct from './pages/UpdateProduct';
import Order from './pages/Order';
import Error1 from './pages/Error1';

import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';
import {useState, useEffect} from 'react';
import {UserProvider} from './UserContext';

function App() {
  const [user, setUser] = useState({  
    id: null,
    isAdmin: null
  });

  const unsetUser = () => {
    localStorage.clear();
  };

  useEffect(() => {
    fetch('https://tranquil-dusk-37263.herokuapp.com/users/log-in-successful', {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {
      console.log(data);

      if(typeof data._id !== "undefined"){
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      } else {
        setUser({
          id: null,
          isAdmin: null
        });
      };
    });
  
  }, []);

  return (
    <>
      <UserProvider value={{user, setUser, unsetUser}}>
        <Router>
            <AppNavBar/>
            <Container>
              <Routes>
                <Route exact path ="/" element={<Home/>}/>
                <Route exact path ="/login" element={<Login/>}/>
                <Route exact path ="/logout" element={<Logout/>}/>
                <Route exact path ="/register" element={<Register/>}/>
                <Route exact path ="/products" element={<Products/>}/>
                <Route exact path ="/productView/:productId" element={<ProductView/>}/>    
                <Route exact path ="/admin" element={<Admin/>}/>   
                <Route exact path ="/profile" element={<Profile/>}/>    
                <Route exact path ="/addProduct" element={<AddProduct/>}/>
                <Route exact path ="/updateProduct/:productId" element={<UpdateProduct/>}/> 
                <Route exact path ="/order" element={<Order/>}/>     
                <Route exact path="*" element={<Error1/>}/>   

              </Routes>         
            </Container>
        </Router>
      </UserProvider>
    </>
  );
}

export default App;
