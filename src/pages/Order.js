import { useContext, useState, useEffect } from "react";
import {Table, Button} from "react-bootstrap";
import {Navigate, Link} from "react-router-dom";
import UserContext from "../UserContext";



export default function Orders(){

	// to validate the user role.
	const {user} = useContext(UserContext);

	const [allOrders, setAllOrders] = useState([]);

	const fetchData = () =>{
		// Get all products in the database
		fetch(`https://tranquil-dusk-37263.herokuapp.com/orders/getAllOrders`,{
			method: "GET",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setAllOrders(data.map(order => {
				return(
					<tr key={order._id}>
						<td>{order._id}</td>
						<td>{order.userId}</td>
						<td>{order._productId}</td>
						<td>{order.totalAmt}</td>
						<td>{order.purchasedOn}</td>
					</tr>
				)
			}))

		})
	}


	
	

	
	useEffect(()=>{
		
		fetchData();
	}, [])

	return(
		(user.isAdmin)
		?
		<>
			<div className="mt-5 mb-3 text-center">
				<h1>Admin Dashboard</h1>
				<Button as={Link} to="/addProduct" variant="success">Add Product</Button>
				<Button as={Link} to="/products" variant="success">Products</Button>
				<Button as={Link} to="/order" variant="success">Orders</Button>
			</div>
			<Table striped bordered hover>
		     <thead>
		       <tr>
		         <th>Orders ID</th>
		         <th>Users</th>
		         <th>Product</th>
		         <th>Total Amount</th>
		         <th>Date Ordered</th>
		       </tr>
		     </thead>
		     <tbody>
		       {allOrders}
		     </tbody>
		   </Table>
		</>
		:
		<Navigate to="/products" />
	)
}
