import ProductCard from '../components/ProductCard';
import {useState, useEffect} from 'react';

export default function Products(){
	const [allProducts, setProducts] = useState([]);

	useEffect(() => {
		fetch("https://tranquil-dusk-37263.herokuapp.com/products/activeProduct")
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setProducts(data.map(product => {
				return (
					<ProductCard key={product._id} productProp = {product}/>
			)
			}));
		});

	}, []);

	return (
		<>
		<h1>Available Products:</h1>		
		{allProducts}
		</>

		)
}