import { Form, Button } from 'react-bootstrap';
import {Navigate, Link} from 'react-router-dom';
import {useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';



export default function Login() {

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(false);
	const {user, setUser} = useContext(UserContext);
	
	function loginUser(e){
		e.preventDefault();

		fetch('https://tranquil-dusk-37263.herokuapp.com/users/log-in-authentication', {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())

		.then(data => {
			console.log(data);

			if(typeof data.accessToken !== "undefined"){
				localStorage.setItem('token', data.accessToken);
				retrieveUserDetails(data.accessToken);

				Swal.fire({
					title: "Login Successful",
					icon: "success",
					text: "Change it later!"
				})

			} else
				{
					Swal.fire({
					title: "Authentication Failed",
					icon: "error",
					text: "Check your credentials"
				})
				}

		});

		setEmail('');
		setPassword('');
			
		
	};

		const retrieveUserDetails = (token) => {

			fetch('https://tranquil-dusk-37263.herokuapp.com/users/log-in-successful', {
					headers: {
						Authorization: `Bearer ${token}`
					}
			})
			.then(res => res.json())
			.then(data => {
				console.log(data);

				setUser({
					id: data._id,
					isAdmin: data.isAdmin
				});
			})
		};

	useEffect(() => {
		if(email !== '' && password !== ''){
			setIsActive(true);
		}
		else{
			setIsActive(false);
		}

	}, [email, password]);



return(
		(user.id !== null)?
			<Navigate to="/"/>
		:
		
		<>
		<h1>Login Here:</h1>
		<Form onSubmit = {e => loginUser(e)}>
			<Form.Group controlId="userEmail">
				<Form.Label>Email Address</Form.Label>
				<Form.Control
					type="email"
					placeholder="Enter your email here"
					required
					value = {email}
					onChange = {e => setEmail(e.target.value)}
				/>
				<Form.Text className="text-muted">
					Log-in with your email
				</Form.Text>
			</Form.Group>

			<Form.Group controlId="password1">
				<Form.Label>Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Enter your password here"
					required
					value = {password}
					onChange = {e => setPassword(e.target.value)}
				/>
			</Form.Group>

			<p>Not yet registered? <Link to="/register">Register Here</Link></p>
			
			{ isActive ? 
				<Button className="mt-3 mb-5" variant="success" type="submit" id="submitBtn">Login</Button>
				:
				<Button className="mt-3 mb-5" variant="info" type="submit" id="submitBtn" disabled>Login</Button>
			}

		</Form>
		</>
    )
}
