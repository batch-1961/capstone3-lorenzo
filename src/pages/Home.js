import Banner from '../components/Banner';
import Highlights from '../components/Highlights';

export default function Home() {
    const data = {
        title: "Change it later",
        content: "change it later",
        destination: "/products",
        label: "check if needed"
    }

    return (
        <>
            <Banner data={data}/>
            <Highlights />
        </>

    )
}
