import { useContext, useState, useEffect } from "react";
import {Table, Button} from "react-bootstrap";
import {useParams, Navigate, Link} from "react-router-dom";
import UserContext from "../UserContext";


import Swal from "sweetalert2";

export default function AdminPage(){

	const {productId} = useParams();
	const {user} = useContext(UserContext);
	const [allProducts, setAllProducts] = useState([]);



	const getData = () =>{
		
		fetch(`https://tranquil-dusk-37263.herokuapp.com/products/allProduct`,{
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setAllProducts(data.map(product => {
				return(
					<tr key={product._id}>
						<td>{product._id}</td>
						<td>{product.name}</td>
						<td>{product.description}</td>
						<td>{product.price}</td>
						<td>{product.quantity}</td>
						<td>{product.isActive ? "Active" : "Inactive"}</td>
						<td>
							{
								(product.isActive)
								?	
								 	
									<Button variant="danger" onClick ={() => archive(product._id, product.name)}>Archive</Button>
								:
									<>
										
										<Button variant="success" onClick ={() => activate(product._id, product.name)}>Activate</Button>
										<Button variant="danger" onClick ={() => deleteProd(product._id)}>Delete</Button>
										<Button as={Link} to={`/updateProduct/${product._id}`} >Edit</Button>
									</>
							}
						</td>
					</tr>
				)
			}))

		})
	}
	//delete product
	const deleteProd = (productId, productName) =>{
		console.log(productId);
		console.log(productName);

		fetch(`https://tranquil-dusk-37263.herokuapp.com/products/deleteProduct/${productId}`,{
			method: "DELETE",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isActive: false
			})
		})
		.then(res => res.json())
		.then(data =>{
			console.log(data);

			if(data){
				Swal.fire({
					title: "Delete Succesful!",
					icon: "success",
					text: `Product is now deleted.`
				})
				getData();
			}
			else{
				Swal.fire({
					title: "Delete Unsuccessful!",
					icon: "error",
					text: `Something went wrong. Please try again later!`
				})
			}
		})
	}


	//archiving product
	const archive = (productId, productName) =>{
		console.log(productId);
		console.log(productName);

		fetch(`https://tranquil-dusk-37263.herokuapp.com/products/archiveProduct/${productId}`,{
			method: "DELETE",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isActive: false
			})
		})
		.then(res => res.json())
		.then(data =>{
			console.log(data);

			if(data){
				Swal.fire({
					title: "Archive Succesful!",
					icon: "success",
					text: `${productName} is now inactive.`
				})
				getData();
			}
			else{
				Swal.fire({
					title: "Archive Unsuccessful!",
					icon: "error",
					text: `Something went wrong. Please try again later!`
				})
			}
		})
	}

	//Making the product active
	const activate = (productId, productName) =>{
		console.log(productId);
		console.log(productName);

		fetch(`https://tranquil-dusk-37263.herokuapp.com/products/activateProduct/${productId}`,{
			method: "PUT",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isActive: true
			})
		})
		.then(res => res.json())
		.then(data =>{
			console.log(data);

			if(data){
				Swal.fire({
					title: "Activate Succesful!",
					icon: "success",
					text: `${productName} is now active.`
				})
				getData();
			}
			else{
				Swal.fire({
					title: "Activate Unsuccessful!",
					icon: "error",
					text: `Something went wrong. Please try again later!`
				})
			}
		})
	}

	useEffect(()=>{
		
		getData();
	}, [])

	return(
		(user.isAdmin)
		?
		<>
			<div className="mt-5 mb-3 text-center">
				<h1>Admin Dashboard</h1>
				{/*A button to add a new product*/}
				<Button as={Link} to="/addProduct" variant="success">Add Product</Button>
				<Button as={Link} to="/products" variant="success">Products</Button>
				<Button as={Link} to="/order" variant="success">Orders</Button>
			</div>
			<Table striped bordered hover dark>
		     <thead>
		       <tr>
		         <th>Product ID</th>
		         <th>Product Name</th>
		         <th>Description</th>
		         <th>Price</th>
		         <th>Status</th>
		         <th>Action</th>
		       </tr>
		     </thead>
		     <tbody>
		       { allProducts }
		     </tbody>
		   </Table>
		</>
		:
		<Navigate to="/admin" />
	)
}
